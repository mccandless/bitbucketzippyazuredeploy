from flask import Flask, redirect
app = Flask(__name__)

@app.route("/")
def hello():
    return redirect("https://www.madjabb.com/lnk_nf.asp?o=8067&c=1&a=279700&l=6779", code=302)
